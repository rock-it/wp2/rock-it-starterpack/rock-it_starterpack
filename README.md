<p align="left">
  <img src="assets/bouquet.png" alt="Logo" width="50%">
</p>

## Overview

Bouquet simplifies the process of defining and running a set of services for a beamline. This software defines and
maintains a set of service classes with predefined behaviors and configurations. This allows a user to define the
desired set of services at a high architectural level, and Bouquet will generate the necessary compose file to run the
services.

It is designed to be used with a beamline directory that contains a `beamline_config.yml` file. This file contains the
configuration for the services that will be run in the Compose configuration.

Each service should only require its name (the corresponding key in services) and its service type. Bouquet will
substitute default values for required composition settings as needed. To overwrite these values you simply enter
key/value pairs into the service just as you would a container composition file (e.g. docker-compose.yml).

Bouquet will automatically generate support services as required (e.g. redis, mariadb, mongodb, etc.)

## Features

- Parses environment variables and command-line arguments to configure the script's behavior.
- Generates a Podman or Docker Compose file based on the provided beamline configuration file.
- Starts the services defined in the beamline configuration file with Podman or Docker Compose.
- Supports a variety of service types, including Bluesky queueservers, QT GUIs, and HTTP APIs, Daiquiri, Flint, Redis,
MariaDB, and MongoDB.
- A beamline configuration file can be created to fully define the services to be run. Bouquet will handle necessary
substitutions and configurations.
- `rely_on_traefik` and `apply_traefik_labels` are two options that enable
easy implementation of traefik. The former will automatically add the traefik reverse proxy service and disable exposed ports of other services. The latter will automatically apply pre-defined labels to the services to configure traefik without needing to mount a traefik.yml config file.

### Example beamline_config.yml
#### A simple example
```yml
beamline_name: default_beamline
description: Default beamline configuration
contact_person: John Doe
contact_email: "myemail@fakeemail.com"
contact_phone: 555-0123
version: '3'
host_ip_address: 127.0.0.1
services:
  queueserver:
    type: queueserver
  queueserver_qtgui:
    type: queueserver_qtgui
  queueserver_http_api:
    type: queueserver_http_api
  bluesky_blissdata:
    type: bluesky_blissdata
  daiquiri:
    type: daiquiri_bluesky
  flint:
    type: flint
```
#### A more complex example
```yml
beamline_name: p65
description: ROCK-IT catalysis demonstrator at Petra-III P65, DESY
contact_person: Devin Burke
contact_email: email@domain.com
contact_phone: "0000"
host_ip_address: "domainname.desy.de"
compose_file: "./podman-compose.yml"
version: '0.2.0'
rely_on_traefik: true
apply_traefik_labels: true

services:
  bluesky-primary:
    type: queueserver
    image: myregistry/beamlineQueue:latest
    device_file: sim_devices.yml
    image: "beamline_p65:latest"
    volumes:
      - /beamlinestorage:/beamlinestorage

  bluesky-secondary:
    type: queueserver
    device_file: sim_devices.yml
    volumes:
      - ./config/bluesky-secondary:/config
      - ./src/p65:/opt/bluesky/

  bluesky-primary-qtgui:
    type: queueserver_qtgui
    title: "Primary Queue Server"
    queueserver: bluesky-primary

  bluesky-secondary-qtgui:
    type: queueserver_qtgui
    title: "Secondary Queue Server"
    queueserver: bluesky-secondary

  bluesky-blissdata:
    type: bluesky_blissdata
    redis: redis_bluesky_blissdata

  daiquiri:
    type: daiquiri_bluesky
    queueserver_http_api: qapi
    redis: redis_bluesky_blissdata
    env_file:
      - ./daiquiri_keys.env
  
  flint:
    type: flint
    redis: redis_bluesky_blissdata

  qapi:
    type: queueserver_http_api
    env_file:
      - ./qapi_keys.env
    queueserver: bluesky-primary

  tiled:
    type: tiled
    env_file:
      - ./tiled_keys.env
    volumes:
      - ./config/tiled:/deploy/config
```

## Supported Service Types

Bouquet recognizes the following service types:
- `queueserver`: A service running a Bluesky kernel and queueserver.
- `queueserver_http_api`: A service running a Bluesky queueserver HTTP API.
- `queueserver_qtgui`: A service running a QT GUI for the Bluesky queueserver.
- `bluesky_blissdata`: A service which translates the Bluesky event model to the Blissdata model.
- `daiquiri_bluesky`: A service running the Daiquiri graphical user interface.
- `flint`: A service running the Flint graphical interface to Blissdata.
- `redis`: A service running a Redis server.
- `mariadb`: A service running a MariaDB server.
- `mongodb`: A service running a MongoDB server.
- `jupyterhub`: A jupyterhub service which spawns JupyterLab instances.
- `traefik`: A reverse-proxy service to securely manage network traffic to and from the pod.
- `whoami`: A tiny Go webserver that prints OS information and HTTP request to output. Useful for development.  

### Service Configuration
Services in Bouquet are meant to be extensions of container composition files (e.g. docker-compose.yml).
This means that you can add any key/value pair that you would normally add to a container composition file to the
service configuration in the beamline configuration file. Bouquet will substitute default values for required
composition settings as needed.

All service types inherit from bouquet.models.`ServiceConfig`

#### Service Config Attributes:
- `container_name` (str): The name of the container, aliased as "name".
- `image` (str): The Docker image to use for the service.
- `restart` (str): The restart policy for the container.
- `environment` (Dict[str, Any]): The environment variables for the container.
- `env_file` (List[str]): List of files containing environment variables.
- `ports` (List[str]): The ports to expose.
- `volumes` (List[str]): The volumes to mount.
- `depends_on` (List[str]): The services this service depends on.
- `networks` (List[str]): The networks the container is connected to.
- `command` (List[str] | str): The command to run in the container.
- `labels` (Dict[str, str]): Labels to apply to the container. Used by some services.

In addition to the standard composition settings, Bouquet recognizes the following service-specific settings:

- `type`: The type of service to run. This is a required field.
- `queueserver`: 
  - device_file (str): Name of the device file in config/bluesky/devices. Default is "devices.yml".
  - redis_host (str): Hostname of the Redis server. Default is "redis".
  - host_ip (str): IP address used to access the service. Default is "127.0.0.1"
- `queueserver_http_api`:
  - queueserver (str): Name of the queueserver service. Default is "queueserver".
  - host_ip (str): IP address used to access the service. Default is "127.0.0.1".
- `queueserver_qtgui`:
  - title (str): Title of the QT GUI window. Default is "".
  - queueserver (str): Name of the queueserver service. Default is "queueserver".
- `bluesky_blissdata`:
  - redis (str): Name of the Redis service. Default is "redis".
  - host_ip (str): IP address used to access the service. Default is "127.0.0.1".
- `daiquiri_bluesky`:
  - redis (str): Name of the Redis service. Default is "redis".
  - mariadb (str): Name of the MariaDB service. Default is "mariadb".
  - queueserver_http_api (str): Name of the queueserver HTTP API service. Default is "queueserver_http_api".
  - host_ip (str): IP address used to access the service. Default is "127.0.0.1"
- `flint`:
  - redis (str): Name of the Redis service. Default is "redis".
  - redis_index (int): Index of the Redis database. Default is 0.
  - host_ip (str): IP address used to access the service. Default is "127.0.0.1".
- `redis`:
  - persist (bool): Flag to persist the Redis database. Default is True.
- `mariadb`:
  - N/A
- `mongodb`:
  - N/A
- `jupyterhub`:
  - host_ip (str): IP address used to access the service. Default is "127.

Generically, bouquet also supports `allow_traefik` and `apply_traefik_labels` to enable connections via Traefik and to apply labels respectively.

## Environment Variables

- `BEAMLINE_DIR`: Path to the host Bluesky directory.
- `BEAMLINE_CONFIG`: Path to the beamline configuration file.
- `COMPOSE_NAME`: Name of the compose file to generate (default: 'podman-compose.yml').
- `COMPOSE_PATH`: Path to the directory where the compose file will be generated. Defaults to the beamline directory.
- `HOST_IP_ADDRESS`: IP address to expose the services (default: '127.0.0.1').
- `USE_DOCKER`: Flag to use Docker instead of Podman (default: False).
- `GENERATE_COMPOSE_ONLY`: Flag to only generate the compose file without starting the services (default: False).
- `DEMO`: Flag to run the demo configuration (default: False).

## Command-Line Arguments

Command-line arguments passed to Bouquet will override environment variables.

- `-b`, `--beamline-dir`: Path to the host beamline directory.
- `-c`, `--beamline-config`: Path to the beamline configuration file.
- `-n`, `--compose-name`: Path to the compose file. Defaults to "./podman-compose.yml".
- `-p`, `--compose-path`: Path to the directory where the compose file will be generated.
- `-d`, `--demo`: Run the demo configuration.
- `-i`, `--host-ip-address`: IP Address to expose the services.
- `-u`, `--use-docker`: When true, uses Docker instead of Podman.
- `-g`, `--generate-compose-only`: Only generate the compose file and do not start the services.
- `-t`, `--apply-traefik-labels`: Apply predefined service labels to configure traefik.


## Usage

To use Bouquet, run the following command:

```sh
bouquet [options] /path/to/beamline_dir-or-config_file
```