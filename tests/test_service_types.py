import pytest
import subprocess
from bouquet.services.types import get_service_config_class
from bouquet.defaults import get_default, change_default
from bouquet.models import ServiceConfig, BeamlineConfig
from bouquet import (
    get_compose_config,
    write_compose_file,
    get_compose_header,
)

ALLOWED_SERVICES = get_default("ALLOWED_SERVICE_TYPES").keys()


def is_valid_podman_compose_file(file_path: str) -> bool:
    try:
        result = subprocess.run(
            ["podman-compose", "-f", file_path, "config"],
            check=True,
            capture_output=True,
            text=True,
        )
        return result.returncode == 0
    except subprocess.CalledProcessError as e:
        print(f"Error: {e.stderr}")
        return False


def get_test_config_dict(service_type):
    return {
        "container_name": service_type,
        "image": get_default("ALLOWED_SERVICE_TYPES")[service_type],
        "environment": {
            "TEST": "test",
        },
        "ports": ["8080:8080"],
        "depends_on": ["dependent1", "dependent2", "dependent3"],
        "networks": ["network"],
    }


testing_beamline_config = {
    "beamline_name": "default_beamline",
    "description": "Default beamline configuration",
    "contact_person": "John Doe",
    "contact_email": "myemail@fakeemail.com",
    "contact_phone": "555-0123",
    "version": get_default("COMPOSE_VERSION"),
    "services": get_default("DEFAULT_SERVICES"),
}


@pytest.mark.parametrize("service_type", ALLOWED_SERVICES)
def test_get_service_config(service_type):
    config_dict = get_test_config_dict(service_type)
    config_class = get_service_config_class(service_type)
    assert issubclass(config_class, ServiceConfig)
    config = config_class(**config_dict)
    assert config.container_name == service_type
    assert config.image == get_default("ALLOWED_SERVICE_TYPES")[service_type]
    # assert the TEST key is in the environment
    if config.environment is not None:
        assert "TEST" in config.environment
    else:
        assert False


def test_beamline_config():
    config = BeamlineConfig(**testing_beamline_config)
    assert config.beamline_name
    assert config.description
    assert config.contact_person
    assert config.contact_email
    assert config.contact_phone
    assert config.version
    assert len(config.services) > 0


def test_get_compose_config():
    beamline_config = BeamlineConfig(**testing_beamline_config)
    compose_config = get_compose_config(beamline_config)
    assert compose_config.version == get_default("COMPOSE_VERSION")
    assert len(compose_config.services) > 0
    assert len(compose_config.networks) > 0
    assert len(compose_config.volumes) > 0
    for _, service in compose_config.services.items():
        assert isinstance(service, ServiceConfig)
    for _, network in compose_config.networks.items():
        assert isinstance(network, dict)
    for _, volume in compose_config.volumes.items():
        assert isinstance(volume, dict)

    service_keys = list(beamline_config.services.keys())
    service_keys.append("redis")
    service_keys.append("mariadb")
    service_keys.append("redis_bluesky_blissdata")
    assert set(compose_config.services.keys()) == set(service_keys)


def test_generate_compose(tmp_path):
    beamline_config = BeamlineConfig(**testing_beamline_config)
    compose_config = get_compose_config(beamline_config)
    filename = tmp_path / "podman-compose.yml"
    header = get_compose_header(
        beamline_config,
        compose_config,
        "test_compose",
        "test_dir",
        "beamline_config.yml",
        "127.0.0.1",
    )
    print(write_compose_file(compose_config, str(filename), header))
    assert filename.exists()
    assert filename.is_file()
    assert is_valid_podman_compose_file(str(filename))


def test_change_default():
    new_compose_version = "0"
    change_default("COMPOSE_VERSION", new_compose_version)
    assert get_default("COMPOSE_VERSION") == new_compose_version
