__all__ = [
    "change_default",
    "get_default",
]


COMPOSE_VERSION = "3"

DEFAULT_BEAMLINE_NAME = "default_beamline"

DEFAULT_VOLUMES = {
    "batches": {
        "labels": {
            "description": "bluesky batches",
        },
    },
    "redis_data": {
        "labels": {
            "description": "Persistent storage for Redis",
        },
    },
}

DEFAULT_NETWORKS = {
    "internal": {
        "driver": "bridge",
        "labels": {
            "description": "Internal network mode. Ports must be manually exposed to the host.",
        },
    }
}

DEFAULT_SERVICES = {
    "queueserver": {
        "type": "queueserver",
    },
    # "queueserver_qtgui": {
    #     "type": "queueserver_qtgui",
    # },
    "queueserver_http_api": {
        "type": "queueserver_http_api",
    },
    "bluesky_blissdata": {"type": "bluesky_blissdata", "redis": "redis_bluesky_blissdata"},
    "daiquiri": {"type": "daiquiri_bluesky", "redis": "redis_bluesky_blissdata"},
    "flint": {"type": "flint", "redis": "redis_bluesky_blissdata"},
    "jupyterhub": {"type": "jupyterhub"},
}

DEFAULT_CONFIGURATION_FILE = "beamline_config.yml"
DEFAULT_COMPOSE_NAME = "./podman-compose.yml"
DEFAULT_HOST_IP_ADDRESS = "127.0.0.1"
DEFAULT_USE_DOCKER = False
DEFAULT_GENERATE_COMPOSE_ONLY = False
ALLOWED_SERVICE_TYPES = {
    "queueserver": "registry.hzdr.de/rock-it/wp2/rock-it-starterpack/rock-it_bluesky:latest",
    "queueserver_qtgui": "registry.hzdr.de/hzb/bluesky/qt_gui/images/minimal-qt-gui-image:v1.0.4",
    "bluesky_blissdata": "gitlab.desy.de:5555/fs-ec/bluesky_blissdata:latest",
    "daiquiri_bluesky": "registry.hzdr.de/rock-it/wp2/rock-it-starterpack/rock-it_daiquiri:latest",
    "flint": "registry.hzdr.de/rock-it/wp2/flint_docker:latest",
    "queueserver_http_api": "registry.hzdr.de/rock-it/wp2/rock-it-starterpack/rock-it_qapi:latest",
    "tiled": "ghcr.io/bluesky/tiled:latest",
    "redis": "hub.docker.com/redis/redis-stack:7.2.0-v5",
    "mongodb": "hub.docker.com/mongo:latest",
    "mariadb": "hub.docker.com/esrfbcu/mimosa-database:main",
    "jupyterhub": "registry.hzdr.de/rock-it/wp2/rock-it-starterpack/rock-it_jupyterhub:latest",
    "traefik": "hub.docker.com/traefik:latest",
    "whoami": "hub.docker.com/containous/whoami:latest",
}

DEFAULT_APPLY_TRAEFIK_LABELS = False

ALL_DEFAULTS = {
    "COMPOSE_VERSION": COMPOSE_VERSION,
    "DEFAULT_BEAMLINE_NAME": DEFAULT_BEAMLINE_NAME,
    "DEFAULT_VOLUMES": DEFAULT_VOLUMES,
    "DEFAULT_NETWORKS": DEFAULT_NETWORKS,
    "DEFAULT_SERVICES": DEFAULT_SERVICES,
    "DEFAULT_CONFIGURATION_FILE": DEFAULT_CONFIGURATION_FILE,
    "DEFAULT_COMPOSE_NAME": DEFAULT_COMPOSE_NAME,
    "DEFAULT_HOST_IP_ADDRESS": DEFAULT_HOST_IP_ADDRESS,
    "DEFAULT_USE_DOCKER": DEFAULT_USE_DOCKER,
    "DEFAULT_GENERATE_COMPOSE_ONLY": DEFAULT_GENERATE_COMPOSE_ONLY,
    "ALLOWED_SERVICE_TYPES": ALLOWED_SERVICE_TYPES,
    "DEFAULT_APPLY_TRAEFIK_LABELS": DEFAULT_APPLY_TRAEFIK_LABELS,
}


def change_default(var_name, new_value):
    ALL_DEFAULTS[var_name] = new_value


def get_default(var_name):
    return ALL_DEFAULTS[var_name]
