from typing import Any
from pydantic import Field
import re

from ...models import ServiceConfig
from ...defaults import get_default


class BlueskyBlissdataServiceConfig(ServiceConfig):
    """
    Configuration for Bluesky Blissdata service.

    This class defines the configuration for the Bluesky Blissdata service,
    which translates the Bluesky event model to the Blissdata model. It
    initializes the service with default values for container name, image,
    restart policy, networks, ports, and environment variables.

    Depends_on: redis

    Attributes:
        container_name (str): The name of the container. Default is
            "bluesky_blissdata".
        image (str): The Docker image to use for the service. Default is the
            image defined in ALLOWED_SERVICE_TYPES.
        restart (str): The restart policy for the container. Default is
            "always".
        networks (list): The networks the container is connected to. Default
            is ["internal"].
        ports (list): The ports to expose. Default is ["127.0.0.1:9037:9032",
            "127.0.0.1:5579:5578"].
        depends_on (list): The services this service depends on. Default is
            ["redis"].
        environment (dict): The environment variables for the container.
            Default includes "redis_host", "redis_port", "zmq_host", and
            "zmq_port".
        redis (str): The Redis service name. Default is "redis".
        host_ip (str): The host IP address where services can be accessed.
            Default is the value of DEFAULT_HOST_IP_ADDRESS.
        allow_traefik (bool): Whether to allow Traefik to manage the service.
            Default is True.
        apply_traefik_labels (bool): Whether to apply Traefik labels to the
            service. Default is the value of

    Methods:
        __init__(config: Dict[str, Any], **kwargs): Initializes the
        configuration with the provided settings and default values.
    """

    redis: str | None = Field(default="redis")
    host_ip: str | None = Field(default=get_default("DEFAULT_HOST_IP_ADDRESS"))
    allow_traefik: bool = Field(default=True)
    apply_traefik_labels: bool = Field(default=get_default("DEFAULT_APPLY_TRAEFIK_LABELS"))

    def __init__(self, **kwargs: Any):
        super().__init__(**kwargs)
        self.host_ip = get_default("DEFAULT_HOST_IP_ADDRESS")
        self.apply_traefik_labels = get_default("DEFAULT_APPLY_TRAEFIK_LABELS")
        self._service_type = "bluesky_blissdata"
        allowed_service_types = get_default("ALLOWED_SERVICE_TYPES")
        if not self.container_name:
            self.container_name = "bluesky_blissdata"
        if not self.image:
            self.image = allowed_service_types["bluesky_blissdata"]
        if not self.restart:
            self.restart = "always"
        if not self.networks:
            self.networks = ["internal"]
        if not self.ports:
            self.ports = [f"{self.host_ip}:9037:9032", f"{self.host_ip}:5579:5578"]

        if not self.depends_on:
            self.depends_on = [self.redis]

        default_environment = {
            "redis_host": self.redis,
            "redis_port": "6379",
            "zmq_host": "localhost",
            "zmq_port": "5578",
        }

        if isinstance(self.environment, dict):
            default_environment.update(self.environment)
        self.environment = default_environment

        # Derive DNS safe names
        safe_container_name = self.container_name
        safe_container_name = safe_container_name.replace("_", "-")
        safe_container_name = re.sub(r"[^a-zA-Z0-9-]", "", safe_container_name)

        beamline_name = get_default("DEFAULT_BEAMLINE_NAME")
        beamline_name = beamline_name.replace("_", "-")
        beamline_name = re.sub(r"[^a-zA-Z0-9-]", "", beamline_name)

        if not self.labels:
            self.labels = {}

        new_labels = (
            {
                "traefik.enable": "true" if self.allow_traefik else "false",
                f"traefik.http.routers.{safe_container_name}.rule": f'Host("{safe_container_name}.{beamline_name}")',
                f"traefik.http.routers.{safe_container_name}.service": safe_container_name,
                f"traefik.http.services.{safe_container_name}.loadbalancer.server.port": "5578",
                f"traefik.http.routers.{safe_container_name}-supervisor.rule": f'Host("{safe_container_name}-supervisor.{beamline_name}")',  # noqa: E501
                f"traefik.http.routers.{safe_container_name}-supervisor.service": f"{safe_container_name}-supervisor",
                f"traefik.http.services.{safe_container_name}-supervisor.loadbalancer.server.port": "9032",
            }
            if self.apply_traefik_labels
            else {}
        )

        for key, value in new_labels.items():
            if key not in self.labels:
                self.labels[key] = value

    class Config:
        extra = "forbid"
